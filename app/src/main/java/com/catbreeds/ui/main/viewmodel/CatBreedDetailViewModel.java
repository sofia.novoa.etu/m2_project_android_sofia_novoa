package com.catbreeds.ui.main.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.catbreeds.data.api.model.CatBreedImageModel;
import com.catbreeds.data.local.entity.CatBreedEntity;
import com.catbreeds.data.repository.CatBreedRepository;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.observers.ResourceSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * CatBreedViewViewModel interagit avec le modèle et prépare également
 * des observables pouvant être observés par l'activité DetailActivity.
 * Elle notifie des changements d'état via des événements de notification de changement
 * à notre DetailActivity.
 * Ce viewmodel utilise LiveData afin de pouvoir gérer les flux de données.
 */
public class CatBreedDetailViewModel extends ViewModel {

    private CatBreedRepository catBreedRepository;
    private CompositeDisposable compositeDisposable;
    private CompositeDisposable compositeDisposableBis;

    private MutableLiveData<CatBreedEntity> mCatBreed;
    private MutableLiveData<List<CatBreedImageModel>> mImages;

    public CatBreedDetailViewModel(CatBreedRepository catBreedRepository){
        this.catBreedRepository = catBreedRepository;
        this.compositeDisposable = new CompositeDisposable();
        this.compositeDisposableBis = new CompositeDisposable();
    }

    public MutableLiveData<CatBreedEntity> getCatBreedById(String id){
        mCatBreed = new MutableLiveData<CatBreedEntity>();
        compositeDisposable.add(catBreedRepository.getCatBreedById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ResourceSingleObserver<CatBreedEntity>() {

                    @Override
                    public void onSuccess(@NonNull CatBreedEntity catBreedEntity) {
                        mCatBreed.setValue(catBreedEntity);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                }));
        return mCatBreed;
    }

    public MutableLiveData<List<CatBreedImageModel>> getImagesByBreedId(String id){
        mImages = new MutableLiveData<List<CatBreedImageModel>>();
        Single<List<CatBreedImageModel>> call = catBreedRepository.getImagesByCatBreedId(id);
        compositeDisposableBis.add(call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<CatBreedImageModel>>() {
                    @Override
                    public void onSuccess(@NonNull List<CatBreedImageModel> catBreedImageResponse) {
                        mImages.setValue(catBreedImageResponse);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                }));
        return mImages;
    }
}
