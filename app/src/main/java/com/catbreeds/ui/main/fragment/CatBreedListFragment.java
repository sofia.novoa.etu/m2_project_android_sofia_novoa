package com.catbreeds.ui.main.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.catbreeds.R;
import com.catbreeds.data.api.model.CatBreedModel;
import com.catbreeds.data.utils.CatBreedDependencyInjection;
import com.catbreeds.ui.main.DetailActivity;
import com.catbreeds.ui.main.adapter.CatBreedAdapter;
import com.catbreeds.ui.main.viewmodel.CatBreedListViewModel;

import java.util.List;

/**
 * Cette classe étend l'interface OnClickItemInterface
 * afin de pouvoir gérer le changement vers le Détail.
 * Cela va gérer l'affichage des races de chats
 */
public class CatBreedListFragment extends Fragment implements OnClickItemInterface{
    public static final String TAB_NAME = "CatBreed List";
    private View rootView;
    private RecyclerView recyclerView;
    private CatBreedAdapter catBreedAdapter;
    private CatBreedListViewModel catBreedListViewModel;
    private OnClickItemInterface onClickItemInterface;

    private ImageView list_icon;
    private ImageView grid_icon;

    private CatBreedListFragment() {
    }

    public static CatBreedListFragment newInstance() {
        return new CatBreedListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecyclerView();
        registerViewModels();
        setupGetBreed();
        setupListenersIconButton();
    }

    private void registerViewModels() {
        catBreedListViewModel = new ViewModelProvider(requireActivity(), CatBreedDependencyInjection.getViewModelFactoryInstance()).get(CatBreedListViewModel.class);
        catBreedListViewModel.getmCatBreeds().observe(getViewLifecycleOwner(), new Observer<List<CatBreedModel>>() {
            @Override
            public void onChanged(List<CatBreedModel> catBreedModelList) {
            catBreedAdapter.bindViewModels(catBreedModelList);
            }
        });

    }

    private void setupGetBreed() {
        catBreedListViewModel.getCatBreeds();
    }

    private void setupRecyclerView() {
        recyclerView = rootView.findViewById(R.id.recycler_view);
        catBreedAdapter = new CatBreedAdapter(this);
        recyclerView.setAdapter(catBreedAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * Cette fonction va initialiser des listeners
     * qui permettront de switcher entre
     * les 2 types d'affichage : Grille et liste
     */
    private void setupListenersIconButton(){
        list_icon = (ImageView) rootView.findViewById(R.id.icon_list);
        grid_icon = (ImageView) rootView.findViewById(R.id.icon_tab);

        list_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            }
        });

        grid_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            }
        });
    }


    /**
     * Permet de démarrer l'activité Detail lors
     * d'un clique sur l'élément concerné
     * @param id : Id du cat breed seléctionné
     */
    @Override
    public void onClickItem(String id) {
        Intent i = new Intent(getActivity(), DetailActivity.class);
        i.putExtra("idCatBreed",id);
        startActivity(i);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
