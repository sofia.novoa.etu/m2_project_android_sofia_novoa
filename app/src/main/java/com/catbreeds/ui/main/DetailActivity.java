package com.catbreeds.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.catbreeds.R;
import com.catbreeds.data.api.model.CatBreedImageModel;
import com.catbreeds.data.local.entity.CatBreedEntity;
import com.catbreeds.data.utils.CatBreedDependencyInjection;
import com.catbreeds.ui.main.viewmodel.CatBreedDetailViewModel;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant l'écran de détail
 */
public class DetailActivity extends AppCompatActivity {
    private TextView catBreedNameOriginTextView;
    private TextView catBreedDescriptionTextView;
    private CatBreedDetailViewModel catBreedDetailViewModel;
    private RatingBar ratingBar_child_friendly;
    private RatingBar ratingBar_affection_level;
    private RatingBar ratingBar_intelligence;
    private View view;
    private Button button_wikipedia;
    private ImageSlider imageSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        catBreedDetailViewModel = new ViewModelProvider(this, CatBreedDependencyInjection.getViewModelFactoryInstance()).get(CatBreedDetailViewModel.class);
        view = findViewById(android.R.id.content).getRootView();
        catBreedNameOriginTextView = findViewById(R.id.cat_breed_name_origin_detailed);
        catBreedDescriptionTextView = findViewById(R.id.cat_breed_description_detailed);
        ratingBar_child_friendly = findViewById(R.id.cat_breed_child_friendly);
        ratingBar_affection_level = findViewById(R.id.cat_breed_affection_level);
        ratingBar_intelligence = findViewById(R.id.cat_breed_intelligence);
        button_wikipedia = findViewById(R.id.button_wikipedia);
        imageSlider = findViewById(R.id.slider);
        this.setupImagesSlides();
        this.setupCatBreedDetail();

    }
    private void setupCatBreedDetail(){
        Intent intent=getIntent();
        String value = intent.getStringExtra("idCatBreed");
        catBreedDetailViewModel.getCatBreedById(value).observe(this, new Observer<CatBreedEntity>() {
            @Override
            public void onChanged(CatBreedEntity catBreedEntity) {

                catBreedNameOriginTextView.setText(catBreedEntity.getName()+", "+catBreedEntity.getOrigin());
                catBreedDescriptionTextView.setText(catBreedEntity.getDescription());
                ratingBar_child_friendly.setRating(catBreedEntity.getChild_friendly());
                ratingBar_affection_level.setRating(catBreedEntity.getAffection_level());
                ratingBar_intelligence.setRating(catBreedEntity.getIntelligence());
                button_wikipedia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent viewIntent =
                                new Intent("android.intent.action.VIEW",
                                        Uri.parse(catBreedEntity.getWikipedia_url()));
                        startActivity(viewIntent);
                    }
                });

            }
        });
    }

    /**
     * Initialisation du slider image
     * des races de chats
     */
    private void setupImagesSlides(){
        Intent intent=getIntent();
        String value = intent.getStringExtra("idCatBreed");
        catBreedDetailViewModel.getImagesByBreedId(value).observe(this, new Observer<List<CatBreedImageModel>>() {
            @Override
            public void onChanged(List<CatBreedImageModel> catBreedImageModels) {
                List<SlideModel> slideModels = new ArrayList<>();
                for(CatBreedImageModel i: catBreedImageModels){
                    slideModels.add(new SlideModel(i.getUrl()));
                }
                imageSlider.setImageList(slideModels,true);
            }
        });
    }
}