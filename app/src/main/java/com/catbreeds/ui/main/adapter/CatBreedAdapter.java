package com.catbreeds.ui.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.catbreeds.R;
import com.catbreeds.data.api.model.CatBreedModel;
import com.catbreeds.ui.main.fragment.OnClickItemInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui va permettre de bind les données et
 * de les afficher dans le recycler view
 */
public class CatBreedAdapter  extends RecyclerView.Adapter<CatBreedAdapter.MyViewHolder> {
    private List<CatBreedModel> catBreedList;
    private OnClickItemInterface onClickItemInterface;

    public CatBreedAdapter(OnClickItemInterface onClickItemInterface) {
        this.catBreedList = new ArrayList<>();
        this.onClickItemInterface = onClickItemInterface;
    }

    public void bindViewModels(List<CatBreedModel> catBreedList) {
        this.catBreedList.clear();
        this.catBreedList.addAll(catBreedList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cat_breed_list_item, parent, false);
        return new MyViewHolder(view, onClickItemInterface);
    }

    @Override
    public void onBindViewHolder(@NonNull CatBreedAdapter.MyViewHolder holder, int position) {
        holder.bind(this.catBreedList.get(position));
        final CatBreedModel catBreed = catBreedList.get(position);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickItemInterface != null) {
                    onClickItemInterface.onClickItem(catBreed.getId());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if(this.catBreedList != null) {
            return this.catBreedList.size();
        }
        return 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView originTextView;
        private ImageView imageView;
        private View view;
        private CatBreedModel catBreedModel;
        CardView item;
        private OnClickItemInterface onClickItemInterface;

        public MyViewHolder(View itemView, final  OnClickItemInterface onClickItemInterface) {
            super(itemView);
            this.view = itemView;
            nameTextView = (TextView)itemView.findViewById(R.id.cat_breed_name);
            originTextView = (TextView)itemView.findViewById(R.id.cat_breed_origin);
            imageView = itemView.findViewById(R.id.cat_breed_image);
            item = itemView.findViewById(R.id.item);
            this.onClickItemInterface = onClickItemInterface;
            setupListeners();
        }

        /**
         * Cette fonction initialise un listener sur le bouton
         * Cela va nous aider à permettre de savoir quelle
         * élément de notre liste a été appuyé grâce à son id
         */
        private void setupListeners() {
            this.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickItemInterface.onClickItem(catBreedModel.getId());
                }
            });
        }


        public void bind(CatBreedModel catBreedModel) {
            this.catBreedModel = catBreedModel;
            nameTextView.setText(catBreedModel.getName());
            originTextView.setText(catBreedModel.getOrigin());
            if(catBreedModel.getImage() != null){
                Glide.with(view)
                        .load(catBreedModel.getImage())
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .centerCrop()
                        .into(imageView);
            }

        }
    }

}
