package com.catbreeds.ui.main.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.catbreeds.data.repository.CatBreedRepository;

/**
 * La création de ViewModel étant assez complexe, ViewModelFactory
 * va s"occuper de la création de nos ViewModels
 */
public class ViewModelFactory implements ViewModelProvider.Factory {

    private final CatBreedRepository catBreedRepository;

    public ViewModelFactory(CatBreedRepository catBreedRepository) {
        this.catBreedRepository = catBreedRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(CatBreedListViewModel.class)) {
            return (T) new CatBreedListViewModel(catBreedRepository);
        }
        if (modelClass.isAssignableFrom(CatBreedDetailViewModel.class)){
            return (T) new CatBreedDetailViewModel(catBreedRepository);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
