package com.catbreeds.ui.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.catbreeds.R;
import com.catbreeds.ui.main.fragment.CatBreedListFragment;

/**
 * Activité qui va permettre
 * d'afficher la liste des races de chats
 */
public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupViewPagersAndTabs();
    }

    private void setupViewPagersAndTabs(){

        ViewPager viewPager = findViewById(R.id.tab_viewpager);
        final CatBreedListFragment catBreedListFragment = CatBreedListFragment.newInstance();

        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return catBreedListFragment;
            }

            @Override
            public int getCount() {
                return 1;
            }
        });
    }
    protected void onDestroy() {
        super.onDestroy();
    }

}