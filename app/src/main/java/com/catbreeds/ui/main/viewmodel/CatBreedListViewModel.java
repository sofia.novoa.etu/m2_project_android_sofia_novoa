package com.catbreeds.ui.main.viewmodel;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.catbreeds.data.api.model.CatBreedModel;
import com.catbreeds.data.local.entity.CatBreedEntity;
import com.catbreeds.data.repository.CatBreedRepository;
import com.catbreeds.data.api.model.CatBreedResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * CatBreedViewModel interagit avec le modèle et prépare également
 * des observables pouvant être observés par le fragment CatBreedListFragment.
 * Elle notifie des changements d'état via des événements de notification de changement
 * à notre CatBreedListFragment.
 * Ce viewmodel utilise LiveData afin de pouvoir gérer les flux de données.
 */
public class CatBreedListViewModel extends ViewModel {

    private CatBreedRepository catBreedRepository;
    private CompositeDisposable compositeDisposable;

    private MutableLiveData<List<CatBreedModel>> mCatBreeds = new MutableLiveData<List<CatBreedModel>>();
    private MutableLiveData<CatBreedEntity> mCatBreed = new MutableLiveData<CatBreedEntity>();

    public CatBreedListViewModel(CatBreedRepository catBreedRepository){
            this.catBreedRepository = catBreedRepository;
            this.compositeDisposable = new CompositeDisposable();
    }

    public MutableLiveData<List<CatBreedModel>> getmCatBreeds() {
        return mCatBreeds;
    }


    public void getCatBreeds() {
        compositeDisposable.clear();
        Single<List<CatBreedResponse>> call = catBreedRepository.getCatBreeds();
        compositeDisposable.add(call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<CatBreedResponse>>() {
                    @Override
                    public void onSuccess(@NonNull List<CatBreedResponse> catBreedListResponse) {
                        List<CatBreedModel> catBreedModels = new ArrayList<>();
                        for(CatBreedResponse catBreedResponse: catBreedListResponse){
                            CatBreedModel catBreedModel = new CatBreedModel();
                            catBreedModel.setId(catBreedResponse.getId());
                            catBreedModel.setName(catBreedResponse.getName());
                            catBreedModel.setOrigin(catBreedResponse.getOrigin());
                            if(catBreedResponse.getImage() != null){
                                catBreedModel.setImage(catBreedResponse.getImage().getUrl());
                            }
                            catBreedModels.add(catBreedModel);
                            addCatBreedLocale(catBreedResponse);
                        }
                        mCatBreeds.setValue(catBreedModels);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        System.out.println(e.toString());
                    }
                }));
    }


    public void addCatBreedLocale(final CatBreedResponse cat){
        compositeDisposable.add(catBreedRepository.addCatBreedLocal(cat).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete(){
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                }));

    }

}
