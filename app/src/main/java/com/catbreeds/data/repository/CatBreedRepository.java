package com.catbreeds.data.repository;

import com.catbreeds.data.api.model.CatBreedImageModel;
import com.catbreeds.data.local.entity.CatBreedEntity;
import com.catbreeds.data.local.CatBreedLocal;
import com.catbreeds.data.api.remote.CatBreedRemote;
import com.catbreeds.data.api.model.CatBreedResponse;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Cette classe va gérer les appels fait vers la base de données
 * ou vers une API à distance.
 */
public class CatBreedRepository {

    private final CatBreedRemote catBreedRemote;
    private final CatBreedLocal catBreedLocal;

    public CatBreedRepository(CatBreedRemote catBreedRemote, CatBreedLocal catBreedLocal){
        this.catBreedRemote = catBreedRemote;
        this.catBreedLocal = catBreedLocal;
    }

    public Single<List<CatBreedResponse>> getCatBreeds(){
        return catBreedRemote.getBreeds();
    }

    public Completable addCatBreedLocal(CatBreedResponse cat){
        CatBreedEntity catBreedEntity = new CatBreedEntity();
        catBreedEntity.setId(cat.getId());
        catBreedEntity.setName(cat.getName());
        catBreedEntity.setOrigin(cat.getOrigin());
        catBreedEntity.setDescription(cat.getDescription());
        catBreedEntity.setTemperament(cat.getTemperament());
        catBreedEntity.setChild_friendly(cat.getChild_friendly());
        catBreedEntity.setIntelligence(cat.getIntelligence());
        catBreedEntity.setAffection_level(cat.getAffection_level());
        catBreedEntity.setWikipedia_url(cat.getWikipedia_url());
        if(cat.getImage() != null){
            catBreedEntity.setImage(cat.getImage().getUrl());
        }
        return this.catBreedLocal.addCatbreed(catBreedEntity);
    }

    public Single<CatBreedEntity> getCatBreedById(String id){
        return catBreedLocal.getCatBreedById(id);
    }

    public Single<List<CatBreedImageModel>> getImagesByCatBreedId(String id){
        return catBreedRemote.getImagesByCatBreedId(id);
    }


}
