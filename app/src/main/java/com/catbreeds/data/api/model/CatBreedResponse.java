package com.catbreeds.data.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Objet désérialisé servant à la réponse
 * de l'API
 */
public class CatBreedResponse {
    private String id;
    private String name;
    private String origin;
    private String description;
    private String temperament;
    private int child_friendly;
    private int affection_level;
    private int intelligence;
    private String wikipedia_url;
    @SerializedName("image")
    private CatBreedImageModel image;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public CatBreedImageModel getImage() {
        return image;
    }

    public void setImage(CatBreedImageModel image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public int getChild_friendly() {
        return child_friendly;
    }

    public void setChild_friendly(int child_friendly) {
        this.child_friendly = child_friendly;
    }

    public int getAffection_level() {
        return affection_level;
    }

    public void setAffection_level(int affection_level) {
        this.affection_level = affection_level;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public String getWikipedia_url() {
        return wikipedia_url;
    }

    public void setWikipedia_url(String wikipedia_url) {
        this.wikipedia_url = wikipedia_url;
    }
}
