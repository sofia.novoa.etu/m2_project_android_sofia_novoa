package com.catbreeds.data.api.remote;

import com.catbreeds.data.api.model.CatBreedImageModel;
import com.catbreeds.data.api.model.CatBreedResponse;


import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface utilisé par Retrofit
 * permettant de créer des requêtes pour appeler l'API
 */
public interface CatBreedApi {

    @GET("breeds")
    Single<List<CatBreedResponse>> getBreeds(@Query("x-api-key") String key);

    @GET("images/search")
    Single<List<CatBreedImageModel>> getImagesByBreedId(@Query("x-api-key") String key, @Query("breed_id") String id, @Query("limit") int nb_picture);
}
