package com.catbreeds.data.api.remote;

import com.catbreeds.data.api.model.CatBreedImageModel;
import com.catbreeds.data.api.model.CatBreedResponse;
import com.catbreeds.data.utils.Credentials;

import java.util.List;

import io.reactivex.Single;

/**
 * Classe servant à appeler
 * les requêtes vers l'API
 */
public class CatBreedRemote {

    private CatBreedApi catBreedApi;

    public CatBreedRemote(CatBreedApi catBreedApi){
        this.catBreedApi = catBreedApi;
    }

    public Single<List<CatBreedResponse>> getBreeds(){
        return catBreedApi.getBreeds(Credentials.API_KEY);
    }

    public Single<List<CatBreedImageModel>> getImagesByCatBreedId(String id){
        return catBreedApi.getImagesByBreedId(Credentials.API_KEY, id, 4);
    }

}
