package com.catbreeds.data.api.model;

/**
 * Cette classe sert de modèle
 * et contiendra les objets qui
 * seront affichés sur la liste
 */
public class CatBreedModel {
    private String id;
    private String name;
    private String origin;
    private String image;


    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }
}
