package com.catbreeds.data.local;

import com.catbreeds.data.local.entity.CatBreedEntity;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Classe pour appeler les requêtes
 * sur la base de données locale
 */
public class CatBreedLocal {

    private CatBreedDatabase catBreedDatabase;
    public CatBreedLocal(CatBreedDatabase catBreedDatabase) {
        this.catBreedDatabase = catBreedDatabase;
    }

    public Completable addCatbreed(CatBreedEntity catBreedEntity){
        return catBreedDatabase.catBreedDao().addCatbreed(catBreedEntity);
    }

    public Single<CatBreedEntity> getCatBreedById(String id){
        return catBreedDatabase.catBreedDao().getCatBreedById(id);
    }
}
