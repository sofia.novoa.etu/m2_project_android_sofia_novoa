package com.catbreeds.data.local;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.catbreeds.data.local.entity.CatBreedEntity;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Classe qui fournit les méthodes
 * pouvant interroger notre base de données locale
 */
@Dao
public interface CatBreedDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    Completable addCatbreed(CatBreedEntity catBreedEntity);

    @Query("SELECT * from CatBreedEntity where id = :id")
    Single<CatBreedEntity> getCatBreedById(String id);
}
