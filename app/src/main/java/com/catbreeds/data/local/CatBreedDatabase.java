package com.catbreeds.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.catbreeds.data.local.entity.CatBreedEntity;

/**
 * Cette classe définit la configuration de la base de données
 * et sert de point d'accès principal de l'application aux données persistantes
 * en base de données
 */
@Database(entities = {CatBreedEntity.class}, version = 1)
public abstract class CatBreedDatabase extends RoomDatabase {
    public abstract CatBreedDao catBreedDao();
}
