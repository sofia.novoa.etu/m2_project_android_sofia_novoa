package com.catbreeds.data.utils;

import android.content.Context;

import androidx.room.Room;

import com.catbreeds.data.api.remote.CatBreedApi;
import com.catbreeds.data.local.CatBreedDatabase;
import com.catbreeds.data.local.CatBreedLocal;
import com.catbreeds.data.api.remote.CatBreedRemote;
import com.catbreeds.data.repository.CatBreedRepository;
import com.catbreeds.ui.main.viewmodel.ViewModelFactory;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Cette classe va servir comme injection de dépendance.
 * En effet, ici nous instancierons des singleton car
 * nous avons besoin à chaque fois d'une unique instance
 * des objets qui seront déclaré pour l'ensemble
 * du cycle de vie de l'application
 */
public class CatBreedDependencyInjection {

    private static Retrofit retrofitInstance;
    private static CatBreedApi catBreedApiInstance;
    private static Gson gson;
    private static ViewModelFactory viewModelFactoryInstance;
    private static CatBreedRepository catBreedRepositoryInstance;
    private static Context applicationContext;
    private static CatBreedDatabase catBreedDatabase;



    public static Retrofit getRetrofitClient() {
        if(retrofitInstance == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();

            retrofitInstance = new Retrofit.Builder()
                    .baseUrl(Credentials.BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();
        }
        return retrofitInstance;
    }

    public static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static CatBreedApi getCatBreedApiInstance(){
        if(catBreedApiInstance == null){
            catBreedApiInstance = getRetrofitClient().create(CatBreedApi.class);
        }
        return catBreedApiInstance;
    }

    public static ViewModelFactory getViewModelFactoryInstance(){
        if(viewModelFactoryInstance == null){
            viewModelFactoryInstance = new ViewModelFactory(getCatBreedRepositoryInstance());
        }
        return viewModelFactoryInstance;
    }

    public static CatBreedRepository getCatBreedRepositoryInstance(){
        if(catBreedRepositoryInstance == null){
            catBreedRepositoryInstance =
                    new CatBreedRepository(
                            new CatBreedRemote(getCatBreedApiInstance()),
                            new CatBreedLocal(getCatBreedDatabase()));
        }
        return catBreedRepositoryInstance;
    }


    public static void setContext(Context context) {
        applicationContext = context;
    }

    public static CatBreedDatabase getCatBreedDatabase() {
        if (catBreedDatabase == null) {
            catBreedDatabase = Room.databaseBuilder(applicationContext, CatBreedDatabase.class, "catbreed-database").build();
        }
        return catBreedDatabase;
    }

}
