package com.catbreeds;

import android.app.Application;
import android.content.Context;

import com.catbreeds.data.utils.CatBreedDependencyInjection;
import com.facebook.stetho.Stetho;

/**
 * Cette classe va permettre de lancer notre application
 * et de faire appelle à notre classe CatBreedDependencyInjection
 * qui va créer des instances uniques d'objets
 */
public class CatBreedApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        CatBreedDependencyInjection.setContext(this);
    }


}
